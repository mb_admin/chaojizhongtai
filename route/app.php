<?php
use think\facade\Route;

Route::get('/', 'index/index');

Route::post('webhook', 'index/webhook');
Route::get('weimeng', 'callback/weimeng');
