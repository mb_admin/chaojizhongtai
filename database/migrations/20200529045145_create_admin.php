<?php

use think\migration\Migrator;
use \Phinx\Db\Adapter\MysqlAdapter;

class CreateAdmin extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $init_data = [
            'username' => 'admin',
            'is_admin' => 1,
            'password' => '$2y$10$RroFfimll8/x2/q0hIm1h.F6mj5bHJ/ZHlEvze3.GeJuKhtP3azIi'
        ];
        $this->table('admin',['engine'=>'InnoDB','comment'=>'管理员','id'=>false,'primary_key'=>'id'])
            ->addColumn('id','integer',['signed'=>false,'identity'=>true,'limit'=>MysqlAdapter::INT_SMALL])
            ->addColumn('username', 'string',['limit'=>20,'comment'=>'管理员编号'])
            ->addColumn('phone', 'char',['default'=>'','limit'=>11,'comment'=>'手机号'])
            ->addColumn('status', 'boolean',['signed'=>false,'default'=>0,'comment'=>'是否停用;0:启用,1:停用'])
            ->addColumn('password', 'char',['limit'=>60,'default'=>0,'comment'=>'密码'])
            ->addColumn('last_login', 'integer',['signed'=>false,'default'=>0,'comment'=>'上次登录时间'])
            ->addColumn('last_ip', 'integer',['signed'=>false,'default'=>0,'comment'=>'上次登录ip'])
            ->addColumn('is_admin', 'boolean',['signed'=>false,'default'=>0,'comment'=>'是否是管理员;0:不是,1:是'])
            ->addColumn('last_update_pwd', 'integer',['signed'=>false,'default'=>0,'comment'=>'上次修改密码时间'])
            ->addColumn('role_id', 'integer',['limit' =>MysqlAdapter::INT_SMALL ,'signed'=>false,'default'=>0,'comment'=>'角色id'])
            ->addColumn('error_count', 'integer',['signed'=>false,'default'=>0,'limit'=>MysqlAdapter::INT_TINY,'comment'=>'错误密码登陆次数'])
            ->addColumn('error_date', 'integer',['signed'=>false,'default'=>0,'comment'=>'错误密码登陆日期'])
            ->addIndex('username',['unique'=>true,'name'=>'username'])
            ->insert($init_data)
            ->create();
    }
}
