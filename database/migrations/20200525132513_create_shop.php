<?php

use think\migration\Migrator;
use Phinx\Db\Adapter\MysqlAdapter;

class CreateShop extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->table('shop', ['id'=>false])
            ->addColumn('id', 'integer', ['limit'=>MysqlAdapter::INT_SMALL,'identity'=>true,'signed'=>false])
            ->addColumn('code', 'string', ['limit'=>20,'comment'=>'网店编码'])
            ->addColumn('name', 'string', ['limit' => 20, 'comment' => '网店名称'])
            ->addColumn('customer_id', 'integer', ['limit' => MysqlAdapter::INT_SMALL,'signed'=>false,'comment' => '客户id'])
            ->addColumn('platform_id', 'integer', ['limit' => MysqlAdapter::INT_TINY, 'signed'=>false,'comment'=>'平台id'])
            ->addColumn('params', 'text', ['comment' => '网店参数'])
            ->addColumn('status', 'boolean', ['signed'=>false,'default'=>0,'comment'=>'状态;0:停用,1:启用'])
            ->addColumn('add_time', 'integer', ['signed'=>false,'default'=>0,'comment'=>'添加时间'])
            ->addColumn('update_time', 'integer', ['signed'=>false,'default'=>0,'comment'=>'修改时间'])
            ->setComment('网店')
            ->setPrimaryKey('id')
            ->addIndex('code', ['name' => 'code', 'unique' => true])
            ->create();
    }
}
