<?php

use think\migration\Migrator;
use \Phinx\Db\Adapter\MysqlAdapter;

class CreateQueueTable extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->table('jobs', ['id'=>false])
            ->addColumn('id', 'integer', ['limit'=>MysqlAdapter::INT_BIG,'signed'=>false,'identity'=>true])
            ->addColumn('queue', 'string', ['limit'=>255,'default'=>'','comment'=>'队列名称'])
            ->addColumn('attempts', 'integer', ['signed'=>false,'default'=>0,'comment'=>'尝试次数'])
            ->addColumn('reserve_time', 'integer', ['signed'=>false,'default'=>0,'null'=>true])
            ->addColumn('available_time', 'integer', ['signed'=>false,'default'=>0])
            ->addColumn('create_time', 'integer', ['signed'=>false, 'default'=>0,'comment'=>'创建时间'])
            ->addColumn('payload', 'text', ['limit'=>MysqlAdapter::TEXT_LONG,'comment'=>'消息'])
            ->setPrimaryKey('id')
            ->setComment('消息队列')
            ->create();
    }
}
