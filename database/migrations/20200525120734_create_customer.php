<?php

use think\migration\Migrator;
use Phinx\Db\Adapter\MysqlAdapter;

class CreateCustomer extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->table('customer',['comment'=>'客户','id'=>false])
            ->addColumn('id', 'integer', ['limit'=>MysqlAdapter::INT_SMALL,'identity'=>true,'signed'=>false])
            ->addColumn('code', 'string', ['limit'=>20,'comment'=>'客户编码'])
            ->addColumn('name', 'string', ['limit' => 20, 'comment' => '客户名称'])
            ->addColumn('status', 'boolean', ['signed'=>false,'default'=>0,'comment'=>'状态;0:停用,1:启用'])
            ->addColumn('add_time', 'integer', ['signed'=>false,'default'=>0,'comment'=>'添加时间'])
            ->addColumn('update_time', 'integer', ['signed'=>false,'default'=>0,'comment'=>'修改时间'])
            ->setPrimaryKey('id')
            ->addIndex('code', ['name'=>'code','unique'=>true])
            ->create();
    }
}
