<?php

use think\migration\Migrator;
use \Phinx\Db\Adapter\MysqlAdapter;

class CreateSchedule extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->table('schedule', ['id'=>false])
            ->addColumn('id', 'integer', ['limit' =>MysqlAdapter::INT_SMALL, 'signed'=>false,'identity'=>true])
            ->addColumn('code', 'string', ['limit'=>50,'default'=>'','comment'=>'编码'])
            ->addColumn('name', 'string', ['limit'=>50,'default'=>'','comment'=>'名称'])
            ->addColumn('cmd', 'string', ['limit'=>50,'default'=>'','comment'=>'指令'])
            ->addColumn('desc', 'string', ['limit'=>150,'default'=>'','comment'=>'描述'])
            ->addColumn('status', 'boolean', ['default'=>0,'comment'=>'状态0:关闭，1:开启'])
            ->addColumn('meanwhile_num', 'integer', ['limit'=>MysqlAdapter::INT_TINY,'signed'=>false,'default'=>1,'comment'=>'并发数'])
            ->addColumn('type', 'integer', ['limit'=>MysqlAdapter::INT_TINY,'default'=>0,'comment'=>'0-循环间隔执行，1-每天定点执行，2-每周定点执行，3-每月定点执行'])
            ->addColumn('value', 'string', ['limit'=>100,'default'=>0,'comment'=>'type对应{循环执行:间隔时间(分钟);定点执行:(天:08:00,10:00,19:00;周:1:11:00;月:09:11:00)}'])
            ->addColumn('add_time', 'integer', ['signed'=>false,'default'=>0,'comment'=>'添加时间'])
            ->addColumn('last_time', 'integer', ['signed'=>false,'default'=>0,'comment'=>'上次运行时间'])
            ->setPrimaryKey('id')
            ->addIndex('code', ['name'=>'code'])
            ->setComment('定时任务')
            ->create();
    }
}
