<?php

use think\migration\Migrator;
use \Phinx\Db\Adapter\MysqlAdapter;

class CreateWeimengOrder extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->table('weimeng_order', ['id'=>false])
            ->addColumn('id', 'integer', ['signed'=>false,'identity'=>true])
            ->addColumn('orderNo', 'integer', ['limit' => MysqlAdapter::INT_BIG,'signed' => false, 'comment'=>'订单号'])
            ->addColumn('pid', 'integer', ['limit' => MysqlAdapter::INT_BIG,'signed' => false, 'comment'=>'商家id'])
            ->addColumn('wid', 'integer', ['limit' => MysqlAdapter::INT_BIG,'signed' => false, 'comment'=>'用户id'])
            ->addColumn('userNickname', 'string',['limit' => 20, 'default'=>'', 'comment'=>'用户昵称'])
            ->addColumn('existingInvoice', 'boolean',['signed' => false, 'default'=>false, 'comment'=>'是否存在发票 （true-存在 false-不存在）'])
            ->addColumn('guiderName', 'string',['limit' => 20, 'default'=>'', 'comment'=>'导购员名字'])
            ->addColumn('orderStatus', 'integer',['limit' => MysqlAdapter::INT_TINY, 'signed'=>false, 'comment'=>'订单状态 (0-待支付,1-待发货,2-已发货,3-已完成,4-已取消)'])
            ->addColumn('orderStatusName', 'string',['limit' => 10, 'signed'=>false, 'comment'=>'订单状态名称'])
            ->addColumn('deliveryType', 'integer',['limit' => MysqlAdapter::INT_TINY, 'default'=>0, 'comment'=>'配送方式 (1-物流配送),2-同城限时达，3-到店自提)'])
            ->addColumn('bizType', 'integer',['limit' => MysqlAdapter::INT_TINY,'default'=>-1,'comment'=>'业务类型 (空为不限,0-普通,3-限时折扣,4-砍价, 5-拼团,19-社区团购)'])
            ->addColumn('subBizType', 'integer',['limit' => MysqlAdapter::INT_TINY,'default'=>-1,'comment'=>'子业务类型 (拼团:1-普通团,2-团内抽奖,3-活动结束抽奖)'])
            ->addColumn('bizOrderId', 'string',['limit' => 100,'default'=>'', 'comment'=>'业务订单id'])
            ->addColumn('confirmReceivedTime', 'integer',['default' => 0,'signed'=>false, 'comment'=>'确认收货/核销时间'])
            ->addColumn('deliveryTime', 'integer',['default' => 0,'signed'=>false, 'comment'=>'发货/备货时间'])
            ->addColumn('enableDelivery', 'boolean',['default' => false,'signed'=>false, 'comment'=>'是否可发货（0-不可发货，1-可发货）*周期购/预售/拼团订单需留意'])
            ->addColumn('deliveryTypeName', 'string',['limit' => 15,'default'=>'', 'comment'=>'配送方式名称'])
            ->addColumn('paymentAmount', 'decimal',['signed' => false,'default'=>0,'precision'=>10,'scale'=>2, 'comment'=>'实付金额'])
            ->addColumn('deliveryAmount', 'decimal',['signed' => false,'default'=>0,'precision'=>8,'scale'=>2, 'comment'=>'运费小计'])
            ->addColumn('channelType', 'integer',['limit'=>MysqlAdapter::INT_TINY,'signed' => false,'default'=>0,'comment'=>'渠道类型 (0-公众号,1-小程序,2-H5,3-QQ,4-微博,5-头条,6-支付宝,7-线下)'])
            ->addColumn('channelTypeName', 'string',['limit'=>15,'default'=>'','comment'=>'渠道类型名称'])
            ->addColumn('paymentType', 'integer',['limit'=>MysqlAdapter::INT_TINY,'default'=>1,'comment'=>'支付类型 (1-线上支付,2-线下支付,3-混合支付)'])
            ->addColumn('paymentTypeName', 'string',['limit'=>15,'default'=>'','comment'=>'支付类型名称'])
            ->addColumn('paymentStatus', 'integer',['limit'=>MysqlAdapter::INT_TINY,'default'=>0,'comment'=>'支付状态 (0-未支付,1-部分支付,2-已支付)'])
            ->addColumn('paymentMethodName', 'string',['limit'=>15,'default'=>'','comment'=>'支付方式名称'])
            ->addColumn('createTime', 'integer',['default' =>0,'signed'=>false, 'comment'=>'下单时间'])
            ->addColumn('updateTime', 'integer',['default' => 0,'signed'=>false, 'comment'=>'更新时间'])
            ->addColumn('paymentTime', 'integer',['default' => 0,'signed'=>false, 'comment'=>'支付时间'])
            ->addColumn('totalPoint', 'integer',['signed' => false,'default' => 0, 'comment'=>'总积分（自提点用）'])
            ->addColumn('transferType', 'integer',['limit'=>MysqlAdapter::INT_TINY,'signed' => false,'default' => 0, 'comment'=>'转单类型：0,不允许转单,1,手动转单,2,按库存自动转单,3,按距离自动转单'])
            ->addColumn('transferStatus', 'integer',['limit'=>MysqlAdapter::INT_TINY,'signed' => false,'default' => 0, 'comment'=>'转单状态：0,未转单,1,已转单,2,转单失败,3,不允许转单'])
            ->addColumn('transferFailReason', 'string',['limit'=>20,'default' => '', 'comment'=>'转单失败原因'])
            ->addColumn('selfPickupSiteName', 'string',['limit'=>20,'default' => '', 'comment'=>'自提点名称'])
            ->addColumn('processStoreTitle', 'string',['limit'=>20,'default' => '', 'comment'=>'服务门店名称'])
            ->addColumn('processStoreId', 'integer',['default' => 0,'signed' => false, 'comment'=>'服务门店id'])
            ->addColumn('storeId', 'integer',['default' => 0,'signed' => false, 'comment'=>'下单门店.id'])
            ->addColumn('storeTitle', 'string',['limit' => 20,'default' => '', 'comment'=>'下单门店名称'])
            ->addColumn('flagRank', 'integer',['limit' => MysqlAdapter::INT_TINY,'signed' => false,'default' => 0, 'comment'=>'订单标记'])
            ->addColumn('flagContent', 'string',['limit' => 20,'default' => '', 'comment'=>'标记文本'])
            ->addColumn('buyerRemark', 'string',['limit' => 200,'default' => '', 'comment'=>'买家备注'])
            ->addColumn('receiverName', 'string',['limit' => 20,'default' => '', 'comment'=>'收货人姓名'])
            ->addColumn('receiverMobile', 'string',['limit' => 15,'default' => '', 'comment'=>'收货人电话'])
            ->addColumn('receiverAddress', 'string',['limit' => 255,'default' => '', 'comment'=>'收货人地址'])
            ->addColumn('expectDeliveryTime', 'string',['limit'=>10,'default' => '', 'comment'=>'配送时间'])
            ->addColumn('deliveryOrderId', 'integer',['limit' => MysqlAdapter::INT_BIG,'default'=>0, 'comment'=>'物流单id'])
            ->addColumn('errcode', 'integer',['limit' => MysqlAdapter::INT_TINY, 'default'=>0,'comment'=>'返回错误码,0表示success'])
            ->addColumn('errmsg', 'string',['limit' => 100,'default'=>'','comment'=>'返回提示信息'])
            ->addColumn('bizSouceType', 'integer',['limit' => MysqlAdapter::INT_TINY,'default'=>0,'comment'=>'1、普通订单，2、app开单，3、收银台开单'])
            ->addColumn('deliveryPaymentAmount', 'decimal',['signed' => false,'default'=>0,'precision'=>8,'scale'=>2, 'comment'=>'运费实付金额'])
            ->addColumn('customer_id', 'integer',['signed' => false,'limit'=>MysqlAdapter::INT_SMALL, 'comment'=>'客户id'])
            ->addColumn('shop_id', 'integer',['signed' => false,'limit'=>MysqlAdapter::INT_SMALL, 'comment'=>'网店id'])
            ->setPrimaryKey('id')
            ->addIndex(['orderNo'],['unique'=>true,'name'=>'orderNo'])
            ->setComment('微盟订单')
            ->create();

        $this->table('weimeng_order_goods', ['id'=>false])
            ->addColumn('iid', 'integer', ['signed'=>false,'identity'=>true])
            ->addColumn('pid', 'integer', ['signed'=>false,'comment'=>'weimeng_order表主键'])
            ->addColumn('rightsStatusName', 'string', ['limit'=>10,'default'=>'','comment'=>'维权状态名称'])
            ->addColumn('rightsStatus', 'integer', ['limit'=>MysqlAdapter::INT_TINY,'default'=>0,'comment'=>'维权状态 (1-维权中,2-已退款)'])
            ->addColumn('rightsOrderId', 'integer', ['limit'=>MysqlAdapter::INT_BIG,'default'=>0,'comment'=>'维权单id'])
            ->addColumn('id', 'integer', ['limit'=>MysqlAdapter::INT_BIG,'default'=>0,'comment'=>'订单项id'])
            ->addColumn('skuAmount', 'decimal',['signed' => false,'default'=>0,'precision'=>10,'scale'=>2, 'comment'=>'sku金额'])
            ->addColumn('totalAmount', 'decimal',['signed' => false,'default'=>0,'precision'=>10,'scale'=>2, 'comment'=>'sku总金额'])
            ->addColumn('paymentAmount', 'decimal',['signed' => false,'default'=>0,'precision'=>10,'scale'=>2, 'comment'=>'实付金额'])
            ->addColumn('goodsId', 'integer',['signed'=>false,'limit'=>MysqlAdapter::INT_BIG,'default'=>0,'comment'=>'商品id'])
            ->addColumn('skuId', 'integer',['signed'=>false,'limit'=>MysqlAdapter::INT_BIG,'default'=>0,'comment'=>'规格id'])
            ->addColumn('goodsTitle', 'string',['limit'=>150,'default'=>'','comment'=>'商品标题'])
            ->addColumn('imageUrl', 'string',['limit'=>150,'default'=>'','comment'=>'规格图片'])
            ->addColumn('skuName', 'string',['limit'=>20,'default'=>'','comment'=>'规格名称'])
            ->addColumn('goodsCategoryId', 'integer',['signed'=>false,'limit'=>MysqlAdapter::INT_BIG,'default'=>0,'comment'=>'商品类目id'])
            ->addColumn('skuNum', 'integer',['signed'=>false,'limit'=>MysqlAdapter::INT_SMALL,'default'=>0,'comment'=>'购买数量'])
            ->addColumn('hadDeliveryItemNum', 'integer',['signed'=>false,'limit'=>MysqlAdapter::INT_SMALL,'default'=>0,'comment'=>'已经发货的数量'])
            ->addColumn('totalPoint', 'integer',['signed'=>false,'default'=>0,'comment'=>'商品总积分（point*skuNum)积分订单用）'])
            ->addColumn('originalPrice', 'decimal',['signed' => false,'default'=>0,'precision'=>10,'scale'=>2, 'comment'=>'原始价'])
            ->addColumn('point', 'integer',['limit'=>MysqlAdapter::BLOB_MEDIUM,'signed' => false,'default'=>0,'comment'=>'商品积分（积分订单用）'])
            ->addColumn('price', 'decimal',['signed' => false,'default'=>0,'precision'=>10,'scale'=>2, 'comment'=>'商品售价'])
            ->addColumn('goodsCode', 'string',['limit' => 30,'default'=>'', 'comment'=>'商品编码'])
            ->addColumn('skuCode', 'string',['limit' => 30,'default'=>'', 'comment'=>'规格编码'])
            ->addColumn('goodsType', 'integer',['limit' => MysqlAdapter::INT_SMALL,'default'=>-1, 'comment'=>'商品类型 (0-普通商品,1-海淘商品)'])
            ->setComment('微盟订单商品')
            ->setPrimaryKey('iid')
            ->addIndex('id', ['name'=>'id'])
            ->create();

        $this->table('weimeng_refund', ['id'=>false])
            ->addColumn('id', 'integer', ['signed'=>false,'identity'=>true])
            ->addColumn('orderNo', 'integer', ['signed'=>false,'limit'=>MysqlAdapter::INT_BIG,'comment'=>'订单号'])
            ->addColumn('refund_id', 'integer', ['signed'=>false,'limit'=>MysqlAdapter::INT_BIG,'comment'=>'售后订单Id'])
            ->addColumn('goodTitle', 'string',['limit'=>100,'default'=>'','comment'=>'商品标题'])
            ->addColumn('skuName', 'string',['limit'=>50,'default'=>'','comment'=>'sku名称'])
            ->addColumn('skuNum', 'integer',['limit'=>MysqlAdapter::INT_SMALL,'signed'=>false,'default'=>1,'comment'=>'sku数量'])
            ->addColumn('price', 'decimal',['signed' => false,'default'=>0,'precision'=>10,'scale'=>2, 'comment'=>'商品单价'])
            ->addColumn('rightsStatus', 'integer',['limit'=>MysqlAdapter::INT_TINY,'default'=>0, 'comment'=>'售后状态 (1-已申请，等待处理,2-等待买家退货,3-买家已退货,5-系统退款中（线上）,6-退款完成,7-用户取消,8-商家拒绝,9-退款失败,10-商家退款中（线下）)'])
            ->addColumn('rightsStatusName','string',['limit'=>10,'default'=>'','comment'=>'售后状态名称'])
            ->addColumn('rightsType','integer',['limit'=>MysqlAdapter::INT_TINY,'default'=>0,'comment'=>'售后类型 (1-售后退货退款,2-退款,3-商品库存不足自动退款,4-商家取消订单自动退款; 2,3,4都认为是退款)'])
            ->addColumn('imageUrl','string',['limit'=>150,'default'=>'','comment'=>'图片地址'])
            ->addColumn('createTime','integer',['signed' => false,'default'=>0,'comment'=>'创建时间'])
            ->addColumn('paymentTime','integer',['signed' => false,'default'=>0,'comment'=>'支付时间'])
            ->addColumn('paymentAmount', 'decimal',['signed' => false,'default'=>0,'precision'=>10,'scale'=>2, 'comment'=>'支付金额'])
            ->addColumn('refundAmount', 'decimal',['signed' => false,'default'=>0,'precision'=>10,'scale'=>2, 'comment'=>'退款金额'])
            ->addColumn('refundBalance', 'decimal',['signed' => false,'default'=>0,'precision'=>10,'scale'=>2, 'comment'=>'退款余额'])
            ->addColumn('refundPoints', 'decimal',['signed' => false,'default'=>0,'comment'=>'退还积分'])
            ->addColumn('refundMethod', 'integer',['limit'=>MysqlAdapter::INT_TINY,'default'=>-1,'comment'=>'退款方式 (0-原路,1-非原路)'])
            ->addColumn('refundType', 'integer',['limit' => MysqlAdapter::INT_TINY,'default'=>0,'comment'=>'退款账号类型 (1-自有,2-微盟代收,3-直连服务商)'])
            ->addColumn('userNickName', 'string',['limit' =>30,'default'=>'','comment'=>'用户昵称'])
            ->addColumn('flagContent', 'string',['limit' =>30,'default'=>'','comment'=>'标记内容'])
            ->addColumn('flagRank', 'integer',['limit' =>MysqlAdapter::INT_TINY,'default'=>-1,'comment'=>'标记等级'])
            ->addColumn('bizType', 'integer',['limit' =>MysqlAdapter::INT_TINY,'default'=>-1,'comment'=>'业务类型 (3-限时折扣,4-砍价,5-拼团,10-特权价,12-N元N件,13-优惠套装,14-加价购)'])
            ->addColumn('storeId', 'integer',['signed' => false,'limit' =>MysqlAdapter::INT_BIG,'default'=>0,'comment'=>'门店id'])
            ->addColumn('storeTitle', 'string',['limit' =>20,'default'=>'','comment'=>'门店名称'])
            ->addColumn('processStoreId', 'integer',['limit'=>MysqlAdapter::INT_BIG,'default'=>0,'comment'=>'处理门店id'])
            ->addColumn('headStoreId', 'integer',['limit'=>MysqlAdapter::INT_BIG,'default'=>0,'comment'=>'主门店id'])
            ->setPrimaryKey('id')
            ->addIndex('refund_id', ['name'=>'refund_id', 'unique'=>true])
            ->addIndex('orderNo', ['name'=>'orderNo'])
            ->create();
    }
}
