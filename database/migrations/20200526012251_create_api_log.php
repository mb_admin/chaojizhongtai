<?php

use think\migration\Migrator;
use Phinx\Db\Adapter\MysqlAdapter;

class CreateApiLog extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->table('api_log', ['id'=>false, 'engine'=>'myisam'])
            ->addColumn('id', 'integer', ['limit'=>MysqlAdapter::INT_BIG,'identity'=>true,'signed'=>false])
            ->addColumn('customer_id', 'integer', ['limit' => MysqlAdapter::INT_SMALL,'signed'=>false,'comment' => '客户id'])
            ->addColumn('platform_id', 'integer', ['limit' => MysqlAdapter::INT_TINY, 'signed'=>false,'comment'=>'平台id'])
            ->addColumn('method', 'string', ['limit' => 100, 'default'=>'','comment'=>'接口名称'])
            ->addColumn('request_params', 'string', ['limit' => 300, 'default'=>'','comment'=>'入参'])
            ->addColumn('response_params', 'text', ['comment'=>'出参'])
            ->addColumn('start_time', 'integer', ['signed'=>false,'default'=>0,'comment'=>'开始时间'])
            ->addColumn('end_time', 'integer', ['signed'=>false,'default'=>0,'comment'=>'结束时间'])
            ->addColumn('status', 'boolean', ['signed'=>false,'default'=>0,'comment'=>'状态;0:失败,1:成功'])
            ->setComment('接口请求日志')
            ->setPrimaryKey('id')
            ->addIndex('customer_id', ['name' => 'customer_id'])
            ->addIndex('start_time', ['name' => 'start_time'])
            ->create();
    }
}
