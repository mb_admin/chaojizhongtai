<?php

use think\migration\Migrator;
use \Phinx\Db\Adapter\MysqlAdapter;

class CreateOut extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $this->table('outer_system',['id'=>false])
            ->addColumn('id','integer',['signed'=>false,'identity'=>true,'limit'=>MysqlAdapter::INT_SMALL])
            ->addColumn('code', 'string',['limit'=>20,'comment'=>'编码'])
            ->addColumn('name', 'string',['default'=>'','limit'=>20,'comment'=>'名称'])
            ->addColumn('status', 'boolean',['signed'=>false,'default'=>0,'comment'=>'是否停用;0:启用,1:停用'])
            ->addColumn('add_time', 'integer', ['signed'=>false,'default'=>0,'comment'=>'添加时间'])
            ->addColumn('update_time', 'integer', ['signed'=>false,'default'=>0,'comment'=>'修改时间'])
            ->addColumn('delete_time', 'integer', ['signed'=>false,'default'=>0,'comment'=>'删除时间'])
            ->setComment('外部系统')
            ->setPrimaryKey('id')
            ->addIndex('code',['unique'=>true,'name'=>'code'])
            ->create();

        $this->table('db_source',['id'=>false])
            ->addColumn('id','integer',['signed'=>false,'identity'=>true,'limit'=>MysqlAdapter::INT_SMALL])
            ->addColumn('code', 'string',['limit'=>20,'comment'=>'编码'])
            ->addColumn('name', 'string',['default'=>'','limit'=>20,'comment'=>'名称'])
//            ->addColumn('customer_id', 'integer',['limit'=>MysqlAdapter::INT_SMALL,'comment'=>'客户id'])
//            ->addColumn('system_id', 'integer',['limit'=>MysqlAdapter::INT_SMALL,'comment'=>'系统id'])
            ->addColumn('type', 'string',['default'=>'mysql','limit'=>20,'comment'=>'数据库类型'])
            ->addColumn('charset', 'string', ['limit'=>10,'default'=>'utf8','comment'=>'字符编码'])
            ->addColumn('host', 'string',['limit' => 20,'default'=>'','comment'=>'服务器地址'])
            ->addColumn('port', 'integer', ['limit'=>MysqlAdapter::INT_MEDIUM,'default'=>3306,'comment'=>'端口'])
            ->addColumn('dbname', 'string', ['limit'=>100,'default'=>'','comment'=>'数据库名称'])
            ->addColumn('username', 'string', ['limit'=>20,'default'=>'','comment'=>'用户名'])
            ->addColumn('password', 'string', ['limit'=>100,'default'=>'','comment'=>'用户密码'])
            ->addColumn('remark', 'text', ['limit'=>0,'comment'=>'备注'])
            ->addColumn('add_time', 'integer', ['signed'=>false,'default'=>0,'comment'=>'添加时间'])
            ->addColumn('update_time', 'integer', ['signed'=>false,'default'=>0,'comment'=>'修改时间'])
            ->addColumn('delete_time', 'integer', ['signed'=>false,'default'=>0,'comment'=>'删除时间'])
            ->setComment('数据源')
            ->setPrimaryKey('id')
            ->addIndex(['code'],['name'=>'code'])
            ->create();
    }
}
