<?php

namespace app\logic\api\platform;

use think\Exception;
use think\facade\Config;

class Weimeng extends BaseApi
{
    protected function initParams($params) : void {
        if (empty($params)) {
            throw new Exception('网店参数未填写');
        }
        if (empty($params['access_token'])) {
            throw new Exception('token参数不能为空');
        }
        $this->publicParams['access_token'] = $params['access_token'];
        $this->url = Config::get('tap.platform.1.request_url');
        $this->access_token = $params['access_token'];
        $this->method = 'post';
    }

    protected function buildParams() : void
    {
        $this->url .= $this->mark . '?accesstoken=' . $this->publicParams['access_token'];
    }

    /**
     * 下载订单
    */
    public function pullOrder(array $params) : array {
        $this->mark = 'order/queryOrderList';
        $this->reqParams = [
            'pageNum' => $params['page'] ?? 1,
            'pageSize' => $params['limit'] ?? 20,
            'queryParameter' => [
                'createStartTime' => date('Y-m-d H:i:s', $params['start_time']),
                'createEndTime' => date('Y-m-d H:i:s', $params['end_time']),
            ]
        ];
        $this->request();
        $rep_data = $this->repData;
        if (isset($rep_data['code']['errcode'])) {
            if ($rep_data['code']['errcode'] == 0) {
                return success('请求成功', $rep_data['data']);
            }
            if ($rep_data['code']['errcode'] > 0) {
                return error($rep_data['code']['errmsg'],[], $rep_data['code']['errcode']);
            }
        }
        return error($this->repContent);
    }

    /**
     * 下载退单
    */
    public function pullRefund(array $params) : array {
        $this->mark = 'rights/searchRightsOrderList';
        $this->reqParams = [
            'pageNum' => $params['page'] ?? 1,
            'pageSize' => $params['limit'] ?? 20,
            'createStartTime' => date('Y-m-d H:i:s', $params['start_time']),
            'createEndTime' => date('Y-m-d H:i:s', $params['end_time']),
        ];
        if (isset($params['orderNo'])) {
            $this->reqParams['orderNo'] = $params['orderNo'];
        }
        if (isset($params['refund_id'])) {
            $this->reqParams['id'] = $params['refund_id'];
        }
        $this->request();
        $rep_data = $this->repData;
        if (isset($rep_data['code']['errcode'])) {
            if ($rep_data['code']['errcode'] == 0) {
                return success('请求成功', $rep_data['data']);
            }
            if ($rep_data['code']['errcode'] > 0) {
                return error($rep_data['code']['errmsg'],[], $rep_data['code']['errcode']);
            }
        }
        return error($this->repContent);
    }

}
