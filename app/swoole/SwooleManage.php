<?php


namespace app\swoole;

use Swoole\Server\Task;
use think\App;
use think\swoole\Manager;

class SwooleManage extends Manager
{

    public function onTask($server, Task $task)
    {
        $data = json_decode($task->data, true);
        if (!isset($data['action']) || !isset($data['params'])) {
            return;
        }
        $this->container->invokeMethod([SwooleTask::class, $data['action']], [$data['params']]);
        $server->finish($task->data);
    }


}
