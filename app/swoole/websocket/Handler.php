<?php


namespace app\swoole\websocket;

use \think\swoole\websocket\socketio\Handler as BaseHandler;
use Swoole\Websocket\Frame;

class Handler extends BaseHandler
{

    public function onMessage(Frame $frame)
    {
        $this->server->task($frame->data);
    }

}
