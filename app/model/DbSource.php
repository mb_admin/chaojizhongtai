<?php
declare (strict_types = 1);

namespace app\model;

use think\Model;

/**
 * @mixin think\Model
 */
class DbSource extends Model
{
    public function OuterSystem()
    {
        return $this->belongsTo('OuterSystem');
    }
    //
    public function customer()
    {
        return $this->belongsTo('Customer','customer_id');
    }
}
