<?php
declare (strict_types = 1);

namespace app\model;

use think\Model;

/**
 * @mixin think\Model
 */
class OuterSystem extends Model
{
    //
    function OuterDb(){
        return $this->hasOne('DbSource','system_id');
    }
}
