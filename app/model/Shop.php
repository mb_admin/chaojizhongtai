<?php
declare (strict_types = 1);

namespace app\model;

use think\Model;
use app\model\Customer;

/**
 * @mixin think\Model
 */
class Shop extends Model
{
    protected $json = ['params'];

    public function customer()
    {
        return $this->belongsTo('Customer','customer_id');
    }
}
