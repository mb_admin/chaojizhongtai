<?php
declare (strict_types = 1);

namespace app\model;

use think\Model;

/**
 * @mixin think\Model
 */
class Barcode extends Model
{
    //
    public function customer()
    {
        return $this->belongsTo('Customer','customer_id');
    }
    
}
