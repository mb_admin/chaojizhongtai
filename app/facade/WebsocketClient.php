<?php

namespace app\facade;

use app\swoole\websocket\Client;
use think\Facade;

/**
 * @see \app\swoole\websocket\Client
 * @mixin \app\swoole\websocket\Client
 */
class WebsocketClient extends Facade
{
    protected static function getFacadeClass() {
        return Client::class;
    }

}
