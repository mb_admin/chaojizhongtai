<?php
// 事件定义文件
return [
    'bind'      => [
        'AdminLogin' =>  \app\service\event\AdminLogin::class
    ],

    'listen'    => [
        'AppInit'  => [],
        'HttpRun'  => [],
        'HttpEnd'  => [],
        'LogLevel' => [],
        'LogWrite' => [],
        'AdminLogin' => [
            \app\service\event\AdminLogin::class
        ]
    ],

    'subscribe' => [
    ],
];
