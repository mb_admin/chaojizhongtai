<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
use think\facade\Route;

Route::group(function() {
    //登录
    Route::post('login','user/login');

    Route::group(function () {
        //资源路由
        Route::resource('customer', 'customer')->only(['index', 'read', 'save', 'update','delete']);
        Route::resource('shop', 'shop')->only(['index', 'read', 'save', 'update','delete']);
        Route::resource('barcode', 'barcode')->only(['index', 'read', 'save', 'update','delete']);
        Route::resource('DbSource', 'DbSource')->only(['index', 'read', 'save', 'update','delete']);
        Route::resource('OuterSystem', 'OuterSystem')->only(['index', 'read', 'save', 'update','delete']);
        Route::resource('schedule', 'schedule')->only(['index', 'read', 'save', 'update','delete']);

        //读取用户信息
        Route::get('user','user/read');
        //退出登录
        Route::get('logout','user/logout');
        //获取配置信息
        Route::get('configList', 'common/configList');
        //获取授权地址
        Route::get('authUrl','authorization/authUrl');
        //刷新token
        Route::get('refreshToken', 'authorization/refreshToken');
        //获取正在运行的计划任务
        Route::get('runList', 'schedule/runList');
        //终止计划任务
        Route::delete('killProcss/<pid>', 'schedule/killProcess');
        //数据库连接测试
        Route::get('testLink', 'DbSource/testLink');
    });
})->allowCrossDomain();
