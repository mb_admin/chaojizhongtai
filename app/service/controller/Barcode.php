<?php
declare (strict_types = 1);

namespace app\service\controller;

use app\BaseController;
use think\facade\Db;
use think\Request;
use app\model\Barcode as Model;
use app\model\Customer as CustomerModel;

class Barcode extends BaseController
{
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index(Request $request)
    {
//        $page = $request->get('page', 1, 'intval');
        $limit = $request->get('limit', 10, 'intval');
        $barcode = $request->get('barcode');
        $sku = $request->get('sku');
        $where = [];
        if (!empty($barcode)) {
            $where['barcode'] = $barcode;
        }
        if (!empty($sku)) {
            $where['sku'] = $sku;
        }
        $users = Model::with('Customer')->where($where)->paginate($limit);
        return $this->pageData($users);
    }

    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function create()
    {
        //
    }

    /**
     * 保存新建的资源
     *
     * @param  \think\Request  $request
     * @return \think\Response
     */
    public function save(Request $request)
    {
        $data = $request->post();
        $this->validate($data, [
            'barcode' => 'require',
            'sku' => 'require',
            'customer_id' => 'require',
        ],[
            'barcode.require' => '条码不能为空',
            'sku.require' => 'sku不能为空',
            'customer_id.require' => '客户不能为空',
        ]);
        $find = Model::whereOr([
            'barcode' => $data['barcode'],
            'sku' => $data['sku'],
        ])
            ->field('barcode,sku')
            ->find();
        if (!empty($find)) {
            if ($find->barcode == $data['barcode']) {
                return $this->error('条码已经存在');
            }
            if ($find->sku == $data['sku']) {
                return $this->error('sku已经存在');
            }
        }
        $customer = CustomerModel::find($data['customer_id']);
        if (empty($customer)) {
            return $this->error('客户不存在!');
        }
        $Barcode = new Model();
        $Barcode->add_time = $request->time();
        $is_success = $Barcode->allowField(['barcode','sku','customer_id'])->save($data);
        if ($is_success === false) {
            return $this->error();
        }
        return $this->success();
    }

    /**
     * 显示指定的资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function read($id)
    {
        $Barcode = Model::find($id);
        if (empty($Barcode)) {
            return $this->error('条码不存在');
        }
        return $this->repData($Barcode);
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * 保存更新的资源
     *
     * @param  \think\Request  $request
     * @param  int  $id
     * @return \think\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->put();
        $model = Model::find($id);
        if (empty($model)) {
            return $this->error('条码不存在');
        }
        $this->validate($data, [
            'barcode' => 'require',
            'sku' => 'require',
            'customer_id' => 'require',
        ],[
            'barcode.require' => '条码不能为空',
            'sku.require' => 'sku不能为空',
            'customer_id.require' => '客户不能为空',
        ]);
        if ($model->barcode != $data['barcode']) {
            $find = Model::where('barcode', $data['barcode'])->field('barcode')->find();
            if (!empty($find)) {
                return $this->error('条码已存在');
            }
        }
        $customer = CustomerModel::find($data['customer_id']);
        if (empty($customer)) {
            return $this->error('客户不存在!');
        }
        $model->update_time = $request->time();
        $is_success = $model->save($data);
        if ($is_success === false) {
            return $this->error();
        }
        return $this->success();
    }

    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function delete($id)
    {
        $Barcode = Model::find($id);
        if (empty($Barcode)) {
            return $this->error('条码不存在');
        }
        $is_success = $Barcode->delete();
        if ($is_success === false) {
            return $this->error();
        }
        return $this->success();
    }
}
