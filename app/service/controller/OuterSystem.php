<?php
declare (strict_types=1);

namespace app\service\controller;

use app\BaseController;
use think\facade\Db;
use think\Request;
use app\model\OuterSystem as Model;

class OuterSystem extends BaseController
{
    /**
     * 列表查询
     * @param Request $request
     * @return \think\response\Json
     * @throws \think\db\exception\DbException
     */
    public function index(Request $request)
    {
        $params = $request->param();
        $page   = isset($params['page']) ? $params['page'] : 1;
        $limit  = isset($params['limit']) ? $params['limit'] : 10;
        $where  = [
            ['delete_time', '=', 0]
        ];
        if (isset($params['code']) && !empty($params['code'])) {
            $where[] = ['code', 'like', "%{$params['code']}%"];
        }
        if (isset($params['name']) && !empty($params['name'])) {
            $where[] = ['name', 'like', "%{$params['name']}%"];
        }
        if (isset($params['status']) && $params['status'] != '') {
            $where[] = ['status', '=', $params['status']];
        }
        $pageLimit = [
            'page'      => $page,
            'list_rows' => $limit,
        ];
        $paginate  = Model::where($where)
            ->order('id', 'desc')
            ->paginate($pageLimit);
        return $this->pageData($paginate);
    }

    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function create()
    {
        //
    }

    /**
     * 平台添加
     *
     * @param Request $request
     * @return \think\response\Json
     */
    public function save(Request $request)
    {
        $params = $request->param();
        $this->validate($params, [
            'code' => 'require',
            'name' => 'require',
        ], [
            'code.require' => '平台代码不能为空',
            'name.require' => '平台名称不能为空',
        ]);
        $otherData = Model::where('code', $params['code'])->find();
        if ($otherData) {
            return $this->error('平台代码已经存在');
        }
        $data   = [
            'code'        => $params['code'],
            'name'        => $params['name'],
            'status'      => isset($params['status']) ? $params['status'] : 0,
            'add_time'    => $request->time(),
            'delete_time' => 0,
        ];
        $model  = new Model();
        $result = $model->save($data);
        if (!$result) {
            return $this->error();
        }
        return $this->success();
    }

    /**
     * 显示指定的资源
     *
     * @param int $id
     * @return \think\Response
     */
    public function read($id)
    {
        $dataModel = Model::find($id);
        if (!$dataModel || $dataModel->delete_time!=0) {
            return $this->error('平台信息不存在！');
        }
        return $this->repData($dataModel);
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param int $id
     * @return \think\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * 更新平台信息
     *
     * @param \think\Request $request
     * @param int $id
     * @return \think\Response
     */
    public function update(Request $request, $id)
    {
        $params    = $request->param();
        $this->validate($params, [
            'name' => 'require',
        ], [
            'name.require' => '平台名称不能为空！',
        ]);
        $dataModel = Model::find($id);
        if (empty($dataModel) || $dataModel->delete_time != 0) {
            return $this->error('平台信息不存在');
        }
        $data   = [
            'id'          => $id,
            'name'        => $params['name'],
            'status'      => isset($params['status']) ? $params['status'] : 0,
            'update_time' => $request->time(),
        ];
        $result = $dataModel->save($data);
        if (!$result) {
            return $this->error();
        }
        return $this->success();
    }

    /**
     * 删除指定资源
     *
     * @param int $id
     * @return \think\Response
     */
    public function delete($id)
    {
        $dataModel = Model::find($id);
        if (!$dataModel || $dataModel->delete_time != 0) {
            return $this->error('平台信息不存在');
        }
        $data   = [
            'id'          => $id,
            'delete_time' => time(),
        ];
        $result = $dataModel->save($data);
        if (!$result) {
            return $this->error();
        }
        return $this->success();
    }
}
