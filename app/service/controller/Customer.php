<?php
declare (strict_types = 1);

namespace app\service\controller;

use app\BaseController;
use think\facade\Db;
use think\Request;
use app\model\Customer as Model;

class Customer extends BaseController
{
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index(Request $request)
    {
        $page = $request->param('page', 1, 'intval');
        $limit = $request->param('limit', 10, 'intval');

        $code = $request->param('code','');
        $name = $request->param('name','');
        $status = $request->param('status','');
        $where = [];
        if (!empty($code)) {
            $where['code'] = $code;
        }
        if (!empty($name)) {
            $where['name'] = $name;
        }
        if($status!=''){
            $where['status'] = $status;
        }
        $pageLimit = [
            'page'      => $page,
            'list_rows' => $limit,
        ];
        $paginate = Model::where($where)
            ->order('id', 'desc')
//            ->field('name,code,status,add_time,')
            ->paginate($pageLimit);
        return $this->pageData($paginate);
    }

    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function create()
    {
        //
    }

    /**
     * 保存新建的资源
     *
     * @param  \think\Request  $request
     * @return \think\Response
     */
    public function save(Request $request)
    {
        $data = $request->param();
        $this->validate($data, [
            'code' => 'require',
            'name' => 'require',
        ],[
            'code.require' => '客户代码不能为空',
            'name.require' => '客户名称不能为空',
        ]);
        $find = Model::whereOr([
            'code' => $data['code'],
            'name' => $data['name'],
        ])
            ->field('name,code')
            ->find();
        if (!empty($find)) {
            if ($find->code == $data['code']) {
                return $this->error('客户代码已经存在');
            }
            if ($find->name == $data['name']) {
                return $this->error('客户名称已经存在');
            }
        }

        $customer = new Model();
        $customer->add_time = $request->time();
        $is_success = $customer->allowField(['code','name','add_time','status'])->save($data);
        if ($is_success === false) {
            return $this->error();
        }
        return $this->success();
    }

    /**
     * 显示指定的资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function read($id)
    {
        $customer = Model::find($id);
        if (empty($customer)) {
            return $this->error('客户不存在');
        }
        return $this->repData($customer);
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * 保存更新的资源
     *
     * @param  \think\Request  $request
     * @param  int  $id
     * @return \think\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->param();
        $model = Model::find($id);
        if (empty($model)) {
            return $this->error('客户不存在');
        }
        $this->validate($data, [
            'code' => 'require',
            'name' => 'require',
        ],[
            'code.require' => '客户代码不能为空',
            'name.require' => '客户名称不能为空',
        ]);
        if ($model->code != $data['code']) {
            $find = Model::where('code', $data['code'])->field('code')->find();
            if (!empty($find)) {
                return $this->error('客户代码已存在');
            }
        }
        $data['update_time'] = $request->time();
        $is_success = $model->save($data);
        if ($is_success === false) {
            return $this->error();
        }
        return $this->success();
    }

    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function delete($id)
    {
        $customer = Model::find($id);
        if (empty($customer)) {
            return $this->error('客户不存在');
        }
        $is_success = $customer->delete();
        if ($is_success === false) {
            return $this->error();
        }
        return $this->success();
    }
}
