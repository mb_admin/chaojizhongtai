<?php
declare (strict_types = 1);

namespace app\service\controller;

use app\BaseController;
use think\facade\Config;

class Common extends BaseController
{
    public function configList() {
        //平台接口配置
        $platform_config = Config::get('tap.platform');
        $platform_list = [];
        foreach ($platform_config as $k => $v) {
            if (in_array($k, [1])) {
                //兼容swoole，http_host在swoole cli模式中无法获取，在控制器中重新赋值
                $v['fix_value']['redirect_uri']['value'] = url('/weimeng', [], true, true)->build();
            }
            $platform_list[$k] = [
                //平台名称
                'name' => $v['name'],
                //平台Id
                'id' => $k,
                //商店参数
                'form_params' => $v['form_params'],
//                //固定值
                'fix_value' => $v['fix_value']
            ];
        }
        $platform_list = array_values($platform_list);
        //数据库类型
        $db_type = Config::get('tap.db_type');
        $config_list = compact('platform_list','db_type');
        return $this->repData($config_list);
    }
}
