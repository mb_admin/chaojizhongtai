<?php
declare (strict_types = 1);

namespace app\service\controller;

use app\BaseController;
use think\facade\Db;
use think\Request;
use app\model\Shop as Model;
use app\model\Customer as CustomerModel;

class Shop extends BaseController
{
    /**
     * 显示资源列表
     *
     * @return \think\Response
     */
    public function index(Request $request)
    {
        $page = $request->param('page', 1, 'intval');
        $limit = $request->param('limit', 10, 'intval');
        $code = $request->get('code','');
        $name = $request->get('name','');
        $status = $request->get('status','');
        $customer_id = $request->get('customer_id','');
        $platform_id = $request->get('platform_id','');
        $where = [];
        if (!empty($code)) {
            $where['code'] = $code;
        }
        if (!empty($name)) {
            $where['name'] = $name;
        }
        if (!empty($customer_id)) {
            $where['customer_id'] = $customer_id;
        }
        if($status!=''){
            $where['status'] = $status;
        }
        if (!empty($platform_id)) {
            $where['platform_id'] = $platform_id;
        }
        $pageLimit = [
            'page'      => $page,
            'list_rows' => $limit,
        ];
        $users = Model::with('Customer')->where($where)->order('id', 'desc')->paginate($pageLimit);
        return $this->pageData($users);
    }

    /**
     * 显示创建资源表单页.
     *
     * @return \think\Response
     */
    public function create()
    {
        //
    }

    /**
     * 保存新建的资源
     *
     * @param  \think\Request  $request
     * @return \think\Response
     */
    public function save(Request $request)
    {
        $data = $request->param();
        $this->validate($data, [
            'code' => 'require',
            'name' => 'require',
            'customer_id' => 'require',
            'platform_id' => 'require',
            'params' => 'require',
        ],[
            'code.require' => '店铺代码不能为空',
            'name.require' => '店铺名称不能为空',
            'customer_id.require' => '客户不能为空',
            'platform_id.require' => '平台不能为空',
        ]);
        $find = Model::whereOr([
            'code' => $data['code'],
            'name' => $data['name'],
        ])
            ->field('name,code')
            ->find();

        if (!empty($find)) {
            if ($find->code == $data['code']) {
                return $this->error('店铺代码已经存在');
            }
            if ($find->name == $data['name']) {
                return $this->error('店铺名称已经存在');
            }
        }
        $customer = CustomerModel::find($data['customer_id']);
        if (empty($customer)) {
            return $this->error('客户不存在');
        }
        $data['add_time']=$request->time();
        $data['params']= $data['params'];
        $data['customer_id']=intval($data['customer_id']);
        $data['platform_id']=intval($data['platform_id']);
        $data['status']=intval($data['status']);
        $Shop = new Model($data);

        $is_success = $Shop->allowField(['code','name','customer_id','platform_id','params','add_time','status'])->save();
        if ($is_success === false) {
            return $this->error();
        }
        return $this->success();
    }

    /**
     * 显示指定的资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function read($id)
    {
        $Shop = Model::find($id);
        if (empty($Shop)) {
            return $this->error('网店不存在');
        }
        return $this->repData($Shop);
    }

    /**
     * 显示编辑资源表单页.
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * 保存更新的资源
     *
     * @param  \think\Request  $request
     * @param  int  $id
     * @return \think\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->param();
        $model = Model::find($id);
        if (empty($model)) {
            return $this->error('网店不存在');
        }
        $this->validate($data, [
            'code' => 'require',
            'name' => 'require',
            'customer_id' => 'require',
            'platform_id' => 'require',
            'params' => 'require',
        ],[
            'code.require' => '网店编码不能为空',
            'name.require' => '网店名称不能为空',
            'customer_id.require' => '客户不能为空',
            'platform_id.require' => '平台不能为空',
            'params.require' => '网店参数不能为空',
        ]);
        if ($model->code != $data['code']) {
            $find = Model::where('code', $data['code'])->field('code')->find();
            if (!empty($find)) {
                return $this->error('网店编码已存在');
            }
        }
        if ($model->customer_id != $data['customer_id']) {
            $customer = CustomerModel::find($data['customer_id']);
            if (empty($customer)) {
                return $this->error('客户不存在');
            }
        }
        $data['params']=$data['params'];
        $model->update_time = $request->time();
        $is_success = $model->save($data);
        if ($is_success === false) {
            return $this->error();
        }
        return $this->success();
    }

    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function delete($id)
    {
        $Shop = Model::find($id);
        if (empty($Shop)) {
            return $this->error('网店不存在');
        }
        $is_success = $Shop->delete();
        if ($is_success === false) {
            return $this->error();
        }
        return $this->success();
    }
}
