<?php
declare (strict_types=1);

namespace app\service\event;

use app\facade\Jwt;
use app\model\User;

class AdminLogin
{

    public function handle(User $user) : string
    {

        // 保存登录时间和ip
        $user->last_login = time();
        $user->last_ip = request()->time();
        $user->save();

        $claim_data = [
            'uid' => $user->id
        ];
        //生成token
        return Jwt::buildToken($claim_data);
    }
}
