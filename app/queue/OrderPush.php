<?php

namespace app\queue;

use think\queue\Job;

class OrderPush extends Queue
{

    /**
     * 根据消息中的数据进行实际的业务处理
     * @param array|mixed    $data     发布任务时自定义的数据
     * @return boolean                 任务执行的结果
     */
    protected function send($data) : bool
    {
        $queue_data = json_decode($data, true);
    }
}