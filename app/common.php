<?php
// 应用公共文件
function success(string $message = '操作成功', $data = [], int $code = 200) : array {
    return compact('message', 'data', 'code');
}

function error(string $message = '操作失败', $data = [], int $code = 400) : array {
    return compact('message', 'data', 'code');
}
