<?php
return [
    //电商平台
    'platform' => [
        1 => [
            //名称
            'name' => '微盟',
            //code
            'code' => 'weimeng',
            //接口类
            'class' => \app\logic\platform\Weimeng::class,
            //接口请求地址
            'request_url' => 'https://dopen.weimob.com/api/1_0/ec/',
            //生成token接口地址
            'token_url' => 'https://dopen.weimob.com/fuwu/b/oauth2/token',
            //网店参数字段
            'form_params' => ['client_id','client_secret','redirect_uri'],
            //固定值
            'fix_value' => [
                //回调地址
                'redirect_uri' => [
                    //值
                    'value' => url('/weimeng', [], true, true)->build(),
                    //前端是否显示复制按钮
                    'is_copy' => 1
                ]
            ],
        ]
    ],
    //数据库类型
    'db_type' => [
        ['name' => 'MySQL', 'value' => 'mysql'],
        ['name' => 'Oracle', 'value' => 'oracle'],
        ['name' => 'SQL Server', 'value' => 'sqlsrv'],
    ]
];